<?php 
require("inc/config.php"); 

if($_POST["submitted"]){
	foreach($_POST["parteien"] as $key => $val){
		mysql_query("UPDATE parteien SET sitze = '".addslashes($val)."' WHERE id = ".addslashes($key)."");
	}
}

$q = mysql_query("SELECT * FROM parteien");
$parteien = array();
while($p = mysql_fetch_object($q)){
	$parteien[] = $p;
}
?>
<!DOCTYPE>
<html>
<head>
	<title>Wahlprognose</title>
	<link rel="stylesheet" href="css/main.css">
	<meta charset="utf-8">
</head>
<body>
	<nav class="head">
		<h2>Adminlogin</h2>
		<button class="admin_login">Admin</button>
	</nav>	
	<section class="edit-data">
		<form action="" method="post">
			<?php foreach($parteien as $p):?>
				<label for=""><?php echo $p->name;?>:</label> 
				<input type="text" value="<?php echo $p->sitze;?>" name="parteien[<?php echo $p->id;?>]"><br>
			<?php endforeach;?>
			<input type="submit" value="speichern" name="submitted"> 
			<button class="close-admin">schließen</button>
		</form>
	</section>
	<section class="claim">
		<h1>Kommunalwahlen 2014</h1>
		<p>
			Wiedereinmal dürfen Sie entscheiden wer unsere Gemeinde führen soll.<br>
			Zögern Sie nicht. Informieren Sie sich über die zur Wahlstehenden Parteien, dessen Politiker <br>
			und geben Sie Ihre Stimme ab. Gehen Sie Wählen!
		</p>
		<img src="/img/kreuz.png" alt="">
	</section>
	<section class="ergebnisse">
		<h1>Die Prognose</h1>
		<p>
			Folgend finden Sie eine Hochrechnung der bisher abgegeben Stimmen, die Liste wird stest aktualisiert damit wir Sie auf dem<br>
			Laufenden halten können. Das Diagramm bezieht sich auf die Stimmenabgaben in Prozent.
		</p>
		<hr>
		<section class="ergebniss_wrapper">
			<?php foreach($parteien as $p):?>
				<label for="" class="balken_label"><?php echo $p->name;?>:</label> 
				<div class="progress">
					<div class="balken <?php echo $p->css_color;?>" style="width: <?php echo $p->sitze;?>%; background: <?php echo $p->css_color;?>;"></div>
				</div>
				<div class="clearer"></div>
			<?php endforeach;?>
		</section>
	</section>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>